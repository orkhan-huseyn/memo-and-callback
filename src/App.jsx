import { useState, useMemo, useEffect, useCallback } from "react";
import { expensiveCalculation } from "./calculate";
import Dummy from "./Dummy";

const App = () => {
  const [count, setCount] = useState(0);
  const [todos, setTodos] = useState([]);

  const calculation = useMemo(() => {
    return expensiveCalculation(count);
  }, [count]);

  const increment = () => {
    setCount((c) => c + 1);
  };

  const addTodo = () => {
    setTodos((t) => [...t, "New Todo"]);
  };

  const fetchTodos = useCallback(async () => {
    const response = await fetch("https://dummyjson.com/todos?limit=3");
    const { todos } = await response.json();
    setTodos(todos.map((t) => t.todo));
  }, []);

  useEffect(() => {
    fetchTodos();
  }, [fetchTodos]);

  return (
    <div>
      <div>
        <h2>My Todos</h2>
        {todos.map((todo, index) => {
          return <p key={index}>{todo}</p>;
        })}
        <button onClick={addTodo}>Add Todo</button>
      </div>
      <hr />
      <div>
        Count: {count}
        <button onClick={increment}>+</button>
        <h2>Expensive Calculation</h2>
        {calculation}
      </div>
      <div>
        <Dummy count={count} />
      </div>
    </div>
  );
};

export default App;
