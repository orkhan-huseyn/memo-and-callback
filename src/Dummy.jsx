import React from "react";

const Dummy = () => {
  console.log("Dummy rerender!");

  return <h1>Dummy component!</h1>;
};

export default React.memo(Dummy);
