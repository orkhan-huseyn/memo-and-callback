const memo = {};

// memoization
export function fibonacci(n) {
  console.log(memo);
  if (n in memo) {
    return memo[n];
  }
  console.log(`fibonacci(${n})`);
  if (n < 2) {
    return 1;
  }
  const result = fibonacci(n - 1) + fibonacci(n - 2);
  memo[n] = result;
  return result;
}
